dwm - dynamic window manager
============================
dwm is an extremely fast, small, and dynamic window manager for X.

# Previews
## Desktop preview
![desktop](assets/desktop-preview.png)

## dwm bar preview
![bar](assets/dwm-bar-preview.png)

## dwmblocks preview
![dwmblocks](assets/dwmblocks-preview.png)

## Tag preview feature
![tag-preview](assets/tag-preview-ss.png)

# Details:
* Fonts used:
1). "Dina" pixelsize: 10
2). "CaskaydiaCove Nerd Font Mono" pixelsize: 21

* Patches used:
1).  Tag Preview
2).  Functional Gaps
3).  Cool Autostart
4).  Status CMD
5).  Attach Side
6).  Bar Height
7).  Alternate Tag Indicator
8).  Bar Padding
9).  Color Bar
10). Center Title
11). Rainbow Tags
